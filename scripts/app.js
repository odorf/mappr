'use strict';

var mapprApp = angular.module('mapprApp', [])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/map/', {
        templateUrl: 'views/map/mapView.html',
        controller: 'MapViewCtrl'
      })
      .when('/signin/', {
        templateUrl: 'views/signin.html',
        controller: 'SignInCtrl'
      })
      .when('/register/', {
        templateUrl: 'views/register.html',
        controller: 'RegisterCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }]);

  mapprApp.factory("sessionState", function() {
    return {
      isAuthenticated: false
    };
  });
