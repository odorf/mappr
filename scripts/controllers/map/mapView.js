
'use strict';

mapprApp.controller('MapViewCtrl', function($scope, $routeParams, $http, sessionState) {
	var $ = function(sel) { return document.querySelector(sel); };
	
	$scope.initMap = function() {
		var currLat, currLong;
		navigator.geolocation.getCurrentPosition(function(position) {
			currLat = position.coords.latitude;
			currLong = position.coords.longitude;

			 var options={
			  elt:document.getElementById('map'),       /*ID of element on the page where you want the map added*/
			  zoom:18,                                  /*initial zoom level of the map*/
			  latLng:{lat:currLat, lng:currLong},  /*center of map in latitude/longitude */
			  mtype:'map',                              /*map type (map)*/
			  bestFitMargin:0,                          /*margin offset from the map viewport when applying a bestfit on shapes*/
			  zoomOnDoubleClick:true                    /*zoom in when double-clicking on map*/
			};

			/*Construct an instance of MQA.TileMap with the options object*/
			window.map = new MQA.TileMap(options);
		});
	};

	$scope.co  = [];
    $scope.cod = [];
    $scope.cnt = 0;
    $scope.dst = 0;

	$scope.addLocation = function(lat, long) {
      $scope.co.push(lat, long);
      $scope.cod.push({"lat": lat, "lng": long});
    };
    


	$scope.calcDist = function() {
      $scope.dst += MQA.Util.arcDistance($scope.cod[0],$scope.cod[1],'m');
      if ($scope.dst > 0.0009) {
        $("#coords").innerHTML = $scope.dst.toFixed(3);
      } else {
        $("#coords").innerHTML ="0.000";
      }
    };
    $scope.watchID = null;

    $scope.startTracking = function() {
      $scope.watchID = navigator.geolocation.watchPosition(function(position) {
        $scope.getLocation(position);
      },
      function(e) {
        $scope.locationError(e);
      },
      {enableHighAccuracy:true,maximumAge:0});
    };

	$scope.stopTracking = function() {
     
      navigator.geolocation.clearWatch($scope.watchID);

    };

    $scope.getLocation = function(position) {
      $scope.cnt++;
      $scope.addLocation(position.coords.latitude, position.coords.longitude);
      window.map.setCenter({lat: position.coords.latitude, lng: position.coords.longitude});
      
      if ($scope.co.length > 2) {
          MQA.withModule('shapes', function() {
            var line = new MQA.LineOverlay();
            line.setShapePoints($scope.co);
            window.map.addShape(line);
          });
      }

      if ($scope.cnt === 2) {
        $scope.calcDist();
        $scope.cnt = 0;
        $scope.cod= [];
      }
	};

	$scope.locationError= function(e) {
		var err = "";
		switch(e.code) {
			case 1:
				err = "Permission Denied";
				break;
			case 2:
				err = "Position Unavailable";
				break;
			case 3:
				err = "Timeout";
				break;
		}
		$("#coords").innerHTML = err;
	};
	/* Timer */
	$scope.timeObj = {
		start: 0,
		end :0,
		state: 0,
		milliseconds: 0,
		timer: 0,
		unixEpoch: 0
	};

	$scope.formatTime = function(ms) {
		var d = new Date(ms + $scope.timeObj.unixEpoch).toGMTString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, '$1');
		var x = String(ms % 1000);
		while (x.length < 3) {
		  x = '0' + x;
		}
		d += '.' + x;
		return d.substr(0, d.length - 1);
	};

	$scope.startStop = function() {
		var t = $scope.timeObj;
		switch (t.state) {
		  case 0:
		    t.start = (+new Date()).valueOf();
		    break;
		  case 1:
		    t.end = (+new Date()).valueOf();
		    break;
		}
		t.state = 1 - t.state;

		if (t.state === 0) {
		  clearInterval(t.timer);
		  t.milliseconds += t.end - t.start;
		  t.timer = t.end = t.start = 0;
		  $scope.displayTime();
		} else {
		  t.timer = setInterval($scope.displayTime, 43);
		}

		return false;
	};

	$scope.resetTime = function() {
		var t = $scope.timeObj;
		if (t.state) {
		  $scope.startStop();
		}

		t.timer = t.milliseconds = t.state = t.end = t.start = 0;
		$scope.displayTime();

		return false;
	};

	$scope.displayTime = function() {
		var t = $scope.timeObj;
		if (t.state) {
		  t.end = (new Date()).valueOf();
		}

		$("#time").innerHTML = $scope.formatTime(t.milliseconds + t.end - t.start);
	};



	$scope.start = function() {
		$("#stats").classList.add("show");
		$("#stats").classList.remove("hide");
		if (!$("#stats").classList.contains("running")) {
			$("#stats").classList.add("running");
			$scope.startTracking();
		} else {
			$("#stats").classList.remove("running");
			$scope.stopTracking();
		}
		$scope.startStop();
	};

	$scope.reset = function(){
		$scope.resetTime();
		$scope.stopTracking();
		$("#coords").innerHTML ="0.00";
		window.map.removeAllShapes();
	};

	if (sessionState.isAuthenticated) {
		//authenticated time to map!
		$scope.initMap();
	} else {
		//not authenticated, we'll redirect to home
		//location.href = "#";
		$scope.initMap();
	}
	

});
