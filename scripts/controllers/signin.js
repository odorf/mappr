'use strict';

mapprApp.controller('SignInCtrl', function($scope, $http, sessionState) {

  $scope.signin = function(){
    //validate via http
    //then redirect to map on success
    //if it works set sessionState.isAuthenticated = true;
    $http.post("/signin", {name: $scope.username, pwd: $scope.password}).success(function(res) {
      switch (res.message) {
        case "success":
          sessionState.isAuthenticated = true;
          location.href = "/#/map";
          break;
        case "failure":
          sessionState.isAuthenticated = false;
          console.log(res.error);
          $scope.error = res.error;
          break;
        default:
          sessionState.isAuthenticated = false;
          break;
      }
    });
    //sessionState.isAuthenticated = true;
    //location.href="/#/map";
  };
  $scope.cancel = function(){
    //validate
    location.href="/#/";
  };
});
