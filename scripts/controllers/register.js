'use strict';

mapprApp.controller('RegisterCtrl', function($scope, $http, sessionState) {

  $scope.cancel = function(){
    location.href="/#/";
  };
  $scope.register = function(){
    //if successful, redirect to maf
    $scope.error = "";
    var err = "Required Fields";
    if (!$scope.username) {
		$scope.error = " ,Username";
    }
    if (!$scope.email){
		$scope.error += " ,email address";
    }
    if (!$scope.password) {
		$scope.error += " ,password";
	}
	if ($scope.error !== "") {
		$scope.error = err + $scope.error;
		return;
	}


    $http.post("/register", {name: $scope.username, email: $scope.email, pwd: $scope.password}).success(function(res){
		switch (res.message) {
			case "success":
				sessionState.isAuthenticated = true;
				location.href = "/#/map";
				break;
			case "failure":
				sessionState.isAuthenticated = false;
				$scope.error = res.error;
				console.log(res.error);
				break;
			default:
				sessionState.isAuthenticated = false;
				break;
		}
    });
    //location.href="/#/map";
  };
});
