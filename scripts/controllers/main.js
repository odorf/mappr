'use strict';

mapprApp.controller('MainCtrl', function($scope, sessionState) {

  $scope.signin = function(){
    location.href="/#/signin";
  };
  $scope.register = function(){
    location.href="/#/register";
  };
  $scope.startMapping = function(){
    //skipping authentication
    sessionState.isAuthenticated = true;
    location.href="/#/map";
  };
});
