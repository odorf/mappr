var express = require("express"),
    app = express(),
    http = require("http"),
    server = http.createServer(app),
    db = require("./db.js"),
    bcrypt = require("bcrypt");

server.listen(process.env.PORT || 8080);


//initialize the db connection
db.initDB();
//init models?
var mongoose = require("mongoose"),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var oUser = new Schema({
    userid : ObjectId,
    username : String,
    email: String,
    pwHash : String
});

var User = mongoose.model("User", oUser);


app.use(express.static(__dirname));
app.use(express.bodyParser());

/*routes*/
app.get("/", function( req, res ) {
  res.sendfile(__dirname + "/index.html");
});

app.post('/signin', function(req, res) {
	var usrnm = req.body.name;
	User.findOne({username: usrnm}, function(err, usrData) {
	    if (usrData === null) {
	        //user does not exist check username
	        res.send({name: usrnm, message: "failure", error: "User does not exist"});

	    } else {
	        //Check if pwd matches
	        bcrypt.compare(req.body.pwd, usrData.pwHash, function(err, result) {
	            if (result) {
	                //sign in success
	                res.send({name: usrnm, message: "success"});
	            } else {
	                if (err) {
	                    res.send({name: usrnm, message: "failure", error: err});
	                } else {
	                    res.send({name: usrnm, message: "failure", error: "Invalid Username or Password"});
	                }
	            }
	        });
	    }
	});
});

app.post("/register", function(req, res) {
	var usrnm = req.body.name;
	User.findOne({username: usrnm}, function(err, usrData) {
        if (usrData === null) {
            //create
            bcrypt.genSalt(10, function(err, salt) {
                
			    bcrypt.hash(req.body.pwd, salt, function(err, hash) {

			        var newUser = new User({ username: usrnm, email: req.body.email, pwHash: hash });
			            newUser.save(function(err) {
			                if (err) {
			                    res.send({name: usrnm, message: "failure", error: err});
			                    return;
			                }
			                res.send({name: usrnm, message: "success"});
			            });
			        });
			    });
                    
		} else {
            //emit to client
            res.send({name: usrnm, message: "failure", error: "User already exists"});
        }
    });
});




